<?php
namespace Megacoders\ListModuleBundle\Controller;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;
use Megacoders\AdminBundle\Entity\ListEntityInterface;
use Megacoders\PageBundle\Entity\Page;
use Megacoders\PageBundle\Model\Layout;

class BaseListController extends AbstractListController
{
    /**
     * @var string[]
     */
    protected $mainBlockRoutes = [];

    /**
     * {@inheritdoc}
     */
    protected function getQueryBuilder($limit = null, $offset = null)
    {
        $entityClass = $this->getModuleParameter('entity');

        if (!$entityClass) {
            throw new \LogicException(sprintf('You must specify entity class', $entityClass));
        }

        if (!is_subclass_of($entityClass, ListEntityInterface::class)) {
            throw new \LogicException(sprintf('%s must implements ListEntityInterface', $entityClass));
        }

        /** @var EntityRepository $repository */
        $repository = $this->getDoctrine()->getRepository($entityClass);
        $queryBuilder = $repository->createQueryBuilder('o', 'o.id');

        if ($orderBy = $this->getModuleParameter('orderBy')) {
            if (!is_array($orderBy)) {
                $orderBy = [$orderBy];
            }

            foreach ($orderBy as $item) {
                list($field, $dir) = is_array($item) ? $item : [$item, 'ASC'];
                $queryBuilder->addOrderBy('o.' .$field, $dir);
            }
        }

        if ($limit > 0) {
            $queryBuilder->setMaxResults($limit);
        }

        if ($offset > 0) {
            $queryBuilder->setFirstResult($offset);
        }

        return $queryBuilder;
    }

    /**
     * {@inheritdoc}
     */
    protected function getDetailsQueryBuilder()
    {
        return $this->getQueryBuilder();
    }

    /**
     * {@inheritdoc}
     */
    protected  function getObjectRequestKey()
    {
        return 'id';
    }

    /**
     * @return string
     */
    protected function getBaseRouteName()
    {
        if (!$this->isMain()) {
            $moduleId = $this->getModuleId();
            $actionId = $this->getActionId();
            $entityClassSerialized = addslashes(
                preg_replace('#.*{(.*)}$#', '$1', serialize(['entity' => $this->getModuleParameter('entity')]))
            );

            $routeKey = base64_encode(implode('#', [$moduleId, $actionId, $entityClassSerialized]));

            if (isset($this->mainBlockRoutes[$routeKey])) {
                return $this->mainBlockRoutes[$routeKey];
            }

            $expr = new Expr();

            /** @var Layout[] $layoutsWithMainArea */
            $layoutsWithMainArea = array_filter(
                $this->get('page.manager.layout_manager')->getAll(),
                function(Layout $layout) {
                    return $layout->getMainArea() !== null;
                }
            );

            $pageAreaOrExpr = $expr->orX();

            foreach ($layoutsWithMainArea as $layout) {
                $pageAreaOrExpr->add($expr->andX()
                    ->add($expr->eq('p.layoutId', $expr->literal($layout->getId())))
                    ->add($expr->eq('pb.areaId', $expr->literal($layout->getMainArea()->getId())))
                );
            }

            /** @var QueryBuilder $queryBuilder */
            $queryBuilder = $this->get('doctrine.orm.entity_manager')
                ->getRepository(Page::class)
                ->createQueryBuilder('p')
                ->leftJoin('p.blocks', 'pb')
                ->leftJoin('pb.block', 'b')
                ->where($expr->neq('p.id', $this->getPage()->getPageId()))
                ->andWhere($expr->eq('b.moduleId', $expr->literal($moduleId)))
                ->andWhere($expr->eq('b.actionId', $expr->literal($actionId)))
                ->andWhere($expr->like('b.parameters', $expr->literal('%' .$entityClassSerialized .'%')))
                ->andWhere($pageAreaOrExpr)
                ->orderBy('p.sortOrder')
                ->setMaxResults(1);

            /** @var Page $pages */
            $page = $queryBuilder->getQuery()->getOneOrNullResult();

            if ($page !== null) {
                $this->mainBlockRoutes[$routeKey] = $page->getRouteName();
                return $this->mainBlockRoutes[$routeKey];
            }
        }

        return parent::getBaseRouteName();
    }
}
