<?php

namespace Megacoders\ListModuleBundle\Controller;

use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Gedmo\Translatable\Query\TreeWalker\TranslationWalker;
use Megacoders\PageBundle\Controller\Module\BaseModuleController;
use Megacoders\PageBundle\Entity\PageBlock;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

abstract class AbstractListController extends BaseModuleController
{
    /**
     * {@inheritdoc}
     */
    public function configureRoutes($baseName, $baseUrl, PageBlock $block)
    {
        $collection = new RouteCollection();

        /** List of objects */
        $collection->add(
            $baseName .'_list',
            new Route($baseUrl .'/{page}', ['page' => 1], ['page' => '\d+'])
        );

        /** Object details */
        $collection->add(
            $baseName .'_details',
            new Route($baseUrl .'/details/{id}', ['_action' => 'details'], ['id' => '\d+'])
        );

        return $collection;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        return $this->listAction($request);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function listAction(Request $request)
    {
        $page = $request->get('page', 1);
        $limit = $this->getModuleParameter('limit');
        $offset = ($page - 1) * $limit;

        $query = $this->getQueryBuilder($limit, $offset)->getQuery();
        $query->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class);

        $paginator = new Paginator($query);
        $count = $paginator->count();

        if ($offset > $count) {
            throw new NotFoundHttpException('Page not found.');
        }

        $list = $paginator->getQuery()->getResult();

        return $this->render('list', [
            'list' => $list,
            'pagination' => $this->getPagination($page, $count),
            'module' => $this
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function detailsAction(Request $request)
    {
        $object = $this->getObject($request);

        $this->addPageMetaTitle($object);
        $this->addPageExtraBreadcrumb($object);

        return $this->render('details', [
            'object' => $object,
            'module' => $this
        ]);
    }

    /**
     * @param int|null $limit
     * @param int|null $offset
     * @return QueryBuilder
     */
    protected abstract function getQueryBuilder($limit = null, $offset = null);

    /**
     * @return QueryBuilder
     */
    protected abstract function getDetailsQueryBuilder();

    /**
     * @return string
     */
    protected abstract function getObjectRequestKey();

    /**
     * @param Request $request
     * @return Object|null
     */
    protected function getObject(Request $request)
    {
        $key = $this->getObjectRequestKey();
        $value = $request->get($key);

        if ($value === null) {
            throw new NotFoundHttpException($this->getObjectNotFoundMessage());
        }

        $queryBuilder = $this->getDetailsQueryBuilder();
        $alias = current($queryBuilder->getRootAliases());

        $queryBuilder
            ->andWhere($alias .'.' .$key .' = :value')
            ->setParameter('value', $value);

        $object = $queryBuilder->getQuery()
                ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class)
                ->getOneOrNullResult();

        if ($object === null) {
            throw new NotFoundHttpException($this->getObjectNotFoundMessage());
        }

        return $object;
    }

    /**
     * @param int $page
     * @param int $count
     * @return array|null
     */
    protected function getPagination($page, $count)
    {
        if (!$this->getModuleParameter('pagination')) {
            return null;
        }

        $limit = $this->getModuleParameter('limit');
        $pagesCount = $limit > 0 ? ceil($count / $limit) : 1;
        $pages = [];

        for ($number = 1; $number <= $pagesCount; $number++) {
            $pages[] = [
                'number' => $number,
                'active' => $number == $page
            ];
        }

        return $pages;
    }

    /**
     * @return string
     */
    protected function getObjectNotFoundMessage()
    {
        return 'Object not found';
    }

    /**
     * @param array $parameters
     * @param int $referenceType
     * @return null|string
     */
    public function generateListUrl(array $parameters = [], $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH)
    {
        return $this->generateMainBlockUrl('list', $parameters, $referenceType);
    }

    /**
     * @param $object
     * @param array $parameters
     * @param int $referenceType
     * @return null|string
     */
    public function generateDetailsUrl($object, array $parameters = [], $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH)
    {
        $key = $this->getObjectRequestKey();
        $accessor = PropertyAccess::createPropertyAccessor();
        $params = array_merge(
            [$key => $accessor->getValue($object, $key)],
            $parameters
        );

        return $this->generateMainBlockUrl('details', $params, $referenceType);
    }

}
